package com.org.testsByMina.douglastests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.BasicConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class AppTest {
	
	private static WebDriver driver = null;
	private static WebDriverWait wait = null;
	
	public void login(String username, String password) {
	    driver.findElement(By.name("email")).sendKeys(username);
	    driver.findElement(By.name("password")).sendKeys(password);
	    driver.findElement(By.name("LoginForm|SubmitChanges")).click();
	}

	@Before
	public void openbrowser() {
	    BasicConfigurator.configure();
	    WebDriverManager.chromedriver().setup();
	    driver = new ChromeDriver();
	    wait = new WebDriverWait(driver, 4);
	}
	
	@After
	public void closebrowser() {
		driver.quit();
	}

	@Test
	//check for login success with correct credentials 
    public  void checkLoginSuccess() {
        driver.navigate().to("https://www.douglas.de/mydouglas/login");
        login("monmon238@yahoo.com","Mrtester2$");
		wait.until(ExpectedConditions.urlToBe("https://www.douglas.de/mydouglas"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//li[contains(text(),'Hallo Max')]")));
    }
	
	@Test
	//check for login failure with correct E-mail and wrong password 
    public  void checkLoginFailureWrongPass() {
        driver.navigate().to("https://www.douglas.de/mydouglas/login");
        login("monmon238@yahoo.com","wrongPassword");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name("LoginForm|Error")));
		String failMessage = driver.findElement(By.name("LoginForm|Error")).getText();
		Assert.assertEquals(failMessage, "Ihre Eingabedaten sind leider fehlerhaft, stimmen Benutzername und Passwort?" );
		String url = driver.getCurrentUrl();
		Assert.assertEquals(url, "https://www.douglas.de/mydouglas/login" );
    }
    
	@Test
	//check for login failure with blank E-mail and blank password 
    public  void checkLoginFailureNoPassNoEmail() {
        driver.navigate().to("https://www.douglas.de/mydouglas/login");
        driver.findElement(By.name("LoginForm|SubmitChanges")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'E-Mail Adresse: Bitte füllen Sie das Feld aus.')]")));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'Passwort: Bitte füllen Sie das Feld aus.')]")));
        String url = driver.getCurrentUrl();
		Assert.assertEquals(url, "https://www.douglas.de/mydouglas/login" );
    }
	
	@Test
	//check for login failure with wrong E-mail and correct password 
    public  void checkLoginFailureWrongEmail() {
        driver.navigate().to("https://www.douglas.de/mydouglas/login");
        login("fake@yahoo.com","Mrtester2$");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name("LoginForm|Error")));
		String failMessage = driver.findElement(By.name("LoginForm|Error")).getText();
		Assert.assertEquals(failMessage, "Ihre Eingabedaten sind leider fehlerhaft, stimmen Benutzername und Passwort?" );
		String url = driver.getCurrentUrl();
		Assert.assertEquals(url, "https://www.douglas.de/mydouglas/login" );
    }
	
	@Test
	//check for login failure with blank E-mail and wrong password 
    public  void checkLoginFailureNoEmail() {
        driver.navigate().to("https://www.douglas.de/mydouglas/login");
        login("","Mrtester2$");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'E-Mail Adresse: Bitte füllen Sie das Feld aus.')]")));
        String url = driver.getCurrentUrl();
		Assert.assertEquals(url, "https://www.douglas.de/mydouglas/login" );
    }
	
	@Test
	//check for login failure with wrong E-mail format and wrong password 
    public  void checkLoginFailureWrongEmailFormat() {
        driver.navigate().to("https://www.douglas.de/mydouglas/login");
        login("monmon238yahoo.com","Mrtester2$");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'E-Mail Adresse: Bitte korrigieren Sie das Format.')]")));
        String url = driver.getCurrentUrl();
		Assert.assertEquals(url, "https://www.douglas.de/mydouglas/login" );
    }
	
	@Test
	//check for login failure with correct E-mail and blank password 
    public  void checkLoginFailureNoPass() {
        driver.navigate().to("https://www.douglas.de/mydouglas/login");
        login("monmon238@yahoo.com","");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'Passwort: Bitte füllen Sie das Feld aus.')]")));
        String url = driver.getCurrentUrl();
		Assert.assertEquals(url, "https://www.douglas.de/mydouglas/login" );
    }
	
	@Test
	//check for login failure with wrong E-mail and wrong password 
    public  void checkLoginFailureWrongEmailWrongPass() {
        driver.navigate().to("https://www.douglas.de/mydouglas/login");
        login("fake@yahoo.com","wrongPassword");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name("LoginForm|Error")));
		String failMessage = driver.findElement(By.name("LoginForm|Error")).getText();
		Assert.assertEquals(failMessage, "Ihre Eingabedaten sind leider fehlerhaft, stimmen Benutzername und Passwort?" );
		String url = driver.getCurrentUrl();
		Assert.assertEquals(url, "https://www.douglas.de/mydouglas/login" );
    }
	
	@Test
	//check for the display of the reset password form when password reset is requested  
    public  void checkPasswordResetForm() {
        driver.navigate().to("https://www.douglas.de/mydouglas/login");
        driver.findElement(By.xpath("//a[contains(text(),'Passwort vergessen?')]")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'Sie haben ihr Passwort vergessen?')]")));
    }
    
}
